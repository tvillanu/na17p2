#!/user/bin/python3

import psycopg2

class DB:
    def connect(self, host, user, password, database):
        try:
            self.db = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (host, database, user, password))
            self.cur = self.db.cursor()
        except psycopg2.Error as e:
            print("Message système :", e)
            return False
        return True

    def disconnect(self):
        try:
            self.db.close()
        except psycopg2.Error as e:
            print("Message système :", e)
            return False
        return True

    def addWine(self, id, name, appellation, year, type, quantity, taste, structure, texture, balance):
        try:
            sql = "INSERT INTO Wine VALUES (%d, '%s', '%s', %d, '%s', %f, '{\"taste\":%d,\"structure\":%d,\"texture\":%d,\"balance\":%d}')" % (id, name, appellation, year, type, quantity, taste, structure, texture, balance)
            self.cur.execute(sql)
            self.db.commit()
        except psycopg2.Error as e:
            self.db.rollback()
            print("Message système :", e)

    def getWines(self):
        try:
            sql = "SELECT Wine.id, Wine.name, Wine.appellation, Wine.year, Wine.type, Wine.quantity, CAST(Wine.evaluation->>'taste' AS INTEGER) AS taste, CAST(Wine.evaluation->>'structure' AS INTEGER) AS structure, CAST(Wine.evaluation->>'texture' AS INTEGER) AS texture, CAST(Wine.evaluation->>'balance' AS INTEGER) AS balance FROM Wine"
            self.cur.execute(sql)
            raw = self.cur.fetchall()
            self.db.commit()
        except psycopg2.Error as e:
            self.db.rollback()
            print("Message système :", e)
        return raw

if __name__ == "__main__":
    print("-----Connexion-----")
    host = "82.231.98.50"
    user = "operator"
    passwd = "azerty"
    dbName = "na17p2"

    db = DB()
    if db.connect(host, user, passwd, dbName):
        print("-----Connexion réussie-----")
        choice = '1'
        while choice == '1' or choice == '2':
            print ("\nPour ajouter un vin à la base, entrez 1")
            print ("Pour voir la liste des vins, entrez 2")
            print ("Pour sortir, entrez autre chose")
            choice = input()
            if choice == '1':
                print("\n-----Ajout d'un vin-----")
                id = int(input("ID: "))
                name = input("Nom: ")
                appellation = input("Appellation: ")
                year = int(input("Millésime: "))
                type = input("Type: ")
                quantity = float(input("Quantité: "))
                taste = int(input("Gout: "))
                structure = int(input("Structure: "))
                texture = int(input("Texture: "))
                balance = int(input("Balance: "))
                db.addWine(id, name, appellation, year, type, quantity, taste, structure, texture, balance)
            if choice == '2':
                print("\n-----Liste des vins-----")
                raw = db.getWines()
                for wine in raw:
                    print(wine[0], wine[1], wine[2], wine[3], wine[4], wine[5], wine[6], wine[7], wine[8], wine[9])

        db.disconnect()
    else:
        print("-----Connexion échouée-----")