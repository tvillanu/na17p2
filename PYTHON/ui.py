#!/user/bin/python3

from tkinter import *
from tkinter import ttk
from script import DB

class UI(Frame):
    def __init__(self, fenetre, **kwargs):
        self.db = DB()
        
        fenetre.title("Exploitation viticole")
        Frame.__init__(self, fenetre, **kwargs)
        self.pack()

        self.connectFrame = Frame(self)
        self.connectFrame.pack(side='left')

        Label(self.connectFrame, text="Host:").grid(row=0, sticky=W)

        self.hostEntry = Entry(self.connectFrame, width=10)
        self.hostEntry.insert(0, '82.231.98.50')
        self.hostEntry.grid(row=0, column=1)

        Label(self.connectFrame, text="Utilisateur:").grid(row=1, sticky=W)

        self.userEntry = Entry(self.connectFrame, width=10)
        self.userEntry.insert(0, 'operator')
        self.userEntry.grid(row=1, column=1)

        Label(self.connectFrame, text="MDP:").grid(row=2, sticky=W)

        self.passwdEntry = Entry(self.connectFrame, width=10)
        self.passwdEntry.insert(0, 'azerty')
        self.passwdEntry.grid(row=2, column=1)

        Label(self.connectFrame, text="Nom DB:").grid(row=3, sticky=W)

        self.dbEntry = Entry(self.connectFrame, width=10)
        self.dbEntry.insert(0, 'na17p2')
        self.dbEntry.grid(row=3, column=1)

        self.connectButton = Button(self.connectFrame, width=15, text='Connecter', command=lambda: self.__connect())
        self.connectButton.grid(row=4, column=0, columnspan=2)

        self.disconnectButton = Button(self.connectFrame, width=15, text='Deconnecter', command=lambda: self.__disconnect())
        self.disconnectButton.grid(row=5, column=0, columnspan=2)

        self.addFrame = Frame(self)
        self.addFrame.pack()

        Label(self.addFrame, text="ID:").grid(row=0, sticky=W)

        self.idSpinBox = Spinbox(self.addFrame, width=19, from_=0, to=9999)
        self.idSpinBox.grid(row=0, column=1)

        Label(self.addFrame, text="Nom:").grid(row=1, sticky=W)

        self.nameEntry = Entry(self.addFrame, width=20, bg='white', fg='grey')
        self.nameEntry.insert(0, 'Nom')
        self.nameEntry.bind("<FocusIn>", self.__handle_focus_in)
        self.nameEntry.grid(row=1, column=1)

        Label(self.addFrame, text="Appellation:").grid(row=0, column=2, sticky=W)

        self.appellationEntry = Entry(self.addFrame, width=20, bg='white', fg='grey')
        self.appellationEntry.insert(0, 'Appellation')
        self.appellationEntry.bind("<FocusIn>", self.__handle_focus_in)
        self.appellationEntry.grid(row=0, column=3)

        Label(self.addFrame, text="Millésime:").grid(row=1, column=2, sticky=W)

        self.yearSpinBox = Spinbox(self.addFrame, width=19, from_=0, to=9999)
        self.yearSpinBox.grid(row=1, column=3)

        Label(self.addFrame, text="Type:").grid(row=0, column=4, sticky=W)

        self.typeEntry = Entry(self.addFrame, width=20, bg='white', fg='grey')
        self.typeEntry.insert(0, 'Type')
        self.typeEntry.bind("<FocusIn>", self.__handle_focus_in)
        self.typeEntry.grid(row=0, column=5)

        Label(self.addFrame, text="Quantité:").grid(row=1, column=4, sticky=W)

        self.quantitySpinBox = Spinbox(self.addFrame, width=19, from_=0, to=9999)
        self.quantitySpinBox.grid(row=1, column=5)
        
        Label(self.addFrame, text="Gout:").grid(row=0, column=6, sticky=W)

        self.tasteSpinBox = Spinbox(self.addFrame, width=19, from_=0, to=5)
        self.tasteSpinBox.grid(row=0, column=7)

        Label(self.addFrame, text="Structure:").grid(row=1, column=6, sticky=W)

        self.structureSpinBox = Spinbox(self.addFrame, width=19, from_=0, to=5)
        self.structureSpinBox.grid(row=1, column=7)

        Label(self.addFrame, text="Texture:").grid(row=0, column=8, sticky=W)

        self.textureSpinBox = Spinbox(self.addFrame, width=19, from_=0, to=5)
        self.textureSpinBox.grid(row=0, column=9)

        Label(self.addFrame, text="Balance:").grid(row=1, column=8, sticky=W)

        self.balanceSpinBox = Spinbox(self.addFrame, width=19, from_=0, to=5)
        self.balanceSpinBox.grid(row=1, column=9)

        self.addButton = Button(self.addFrame, text='Ajouter', command=lambda: self.db.addWine(int(self.idSpinBox.get()), self.nameEntry.get(), self.appellationEntry.get(), int(self.yearSpinBox.get()), self.typeEntry.get(), float(self.quantitySpinBox.get()), int(self.tasteSpinBox.get()), int(self.structureSpinBox.get()), int(self.textureSpinBox.get()), int(self.balanceSpinBox.get())))
        self.addButton.grid(row=0, column=10, rowspan=2)

        self.tableFrame = Frame(self)
        self.tableFrame.pack()

        self.tab = ttk.Treeview(self.tableFrame, columns=(1,2,3,4,5,6,7,8,9,10), show="headings")
        self.tab.pack(fill=X, side='left')
        self.tab.heading(1, text='ID')
        self.tab.heading(2, text='Nom')
        self.tab.heading(3, text='Appellation')
        self.tab.heading(4, text='Millésime')
        self.tab.heading(5, text='Type')
        self.tab.heading(6, text='Quantité')
        self.tab.heading(7, text='Gout')
        self.tab.heading(8, text='Structure')
        self.tab.heading(9, text='Texture')
        self.tab.heading(10, text='Balance')

        self.tab.column(1, width = 125)
        self.tab.column(2, width = 125)
        self.tab.column(3, width = 125)
        self.tab.column(4, width = 125)
        self.tab.column(5, width = 125)
        self.tab.column(6, width = 125)
        self.tab.column(7, width = 125)
        self.tab.column(8, width = 125)
        self.tab.column(9, width = 125)
        self.tab.column(10, width = 125)

        self.scroll = ttk.Scrollbar(self.tableFrame, orient="vertical", command=self.tab.yview)
        self.scroll.pack(side = 'right', fill = 'y')

        self.tab.configure(yscrollcommand=self.scroll.set)

        self.refreshButton = Button(self, text='Recharger', command=lambda: self.__winesToTable())
        self.refreshButton.pack(fill=X, side='bottom')

        self.__uiState(False)

        self.mainloop()

    def __handle_focus_in(self, event):
        event.widget.delete(0, END)
        event.widget.config(fg='black')

    def __connect(self):
        if self.db.connect(self.hostEntry.get(), self.userEntry.get(), self.passwdEntry.get(), self.dbEntry.get()):
            self.__uiState(True)

    def __disconnect(self):
        if self.db.disconnect():
            self.__uiState(False)

    def __winesToTable(self):
        self.tab.delete(*self.tab.get_children())
        raw = self.db.getWines()
        for table in raw:
            self.tab.insert('', 'end', values = (table[0], table[1], table[2], table[3], table[4], table[5], table[6], table[7], table[8], table[9]))

    def __uiState(self, bool):
        if bool:
            stateConnect = 'disable'
            state = 'normal'
        else:
            stateConnect = 'normal'
            state = 'disable'
        self.connectButton.configure(state=stateConnect)
        self.disconnectButton.configure(state=state)
        self.hostEntry.configure(state=stateConnect)
        self.userEntry.configure(state=stateConnect)
        self.passwdEntry.configure(state=stateConnect)
        self.dbEntry.configure(state=stateConnect)
        for child in self.addFrame.winfo_children():
            child.configure(state=state)
        self.refreshButton.configure(state=state)

if __name__ == "__main__":
    fenetre = Tk()
    interface = UI(fenetre)