# Application Python

* Le fichier "script" permet d'ajouter et d'afficher des vins avec une CLI
* Le fichier "ui" permet de faire la même chose mais avec une GUI en utilisant le paquet tkinter (sudo apt install python3-tk)

La base de données est actuellement hébergée sur un serveur personnel à l'adresse IP 82.231.98.50 (dans un conteneur Docker, entre autres, grâce à l'API04...)