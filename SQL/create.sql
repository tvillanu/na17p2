CREATE TABLE GrapeVariety (
    name TEXT PRIMARY KEY,
    description TEXT,
    color TEXT
);

CREATE TABLE SoilManagement (
    type TEXT PRIMARY KEY,
    description TEXT
);

CREATE TABLE PruneManagement (
    type TEXT PRIMARY KEY,
    description TEXT
);

CREATE TABLE Wine (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    appellation TEXT,
    year INTEGER NOT NULL,
    type TEXT NOT NULL,
    quantity FLOAT NOT NULL,
    evaluation JSON NOT NULL
);

CREATE TABLE Parcel (
    id INTEGER PRIMARY KEY,
    soilType TEXT NOT NULL,
    surface FLOAT NOT NULL,
    exposure TEXT NOT NULL,
    year INTEGER NOT NULL,
    nameGrapeVariety TEXT REFERENCES GrapeVariety(name) NOT NULL,
    typeSoilManagement TEXT REFERENCES SoilManagement(type) NOT NULL,
    typePruneManagement TEXT REFERENCES PruneManagement(type) NOT NULL,
    idWine INTEGER REFERENCES Wine(id)
);

CREATE TABLE Treatment (
    name TEXT PRIMARY KEY,
    description TEXT
);

CREATE TABLE TreatmentAssociation (
    name TEXT REFERENCES Treatment(name),
    id INTEGER REFERENCES Parcel(id),
    PRIMARY KEY (name, id)
);

CREATE TABLE ClimaticEvent (
    date DATE PRIMARY KEY,
    types JSON NOT NULL
);

CREATE TABLE ClimaticEventAssociation (
    date DATE REFERENCES ClimaticEvent(date),
    id INTEGER REFERENCES Parcel(id),
    intensity INTEGER NOT NULL,
    CHECK (intensity>=0 and intensity<=5),
    PRIMARY KEY (date, id)
);

CREATE TYPE PACKAGETYPE as enum ('WineBottle', 'BoxWine');

CREATE TABLE PackagedWine (
    id INTEGER PRIMARY KEY,
    price FLOAT NOT NULL,
    description TEXT,
    volume FLOAT NOT NULL,
    shape TEXT,
    color TEXT,
    size INTEGER,
    type PACKAGETYPE NOT NULL,
    idWine INTEGER REFERENCES Wine(id) NOT NULL,
    CHECK ((shape IS NOT NULL and color IS NOT NULL and size IS NULL)
            or (shape IS NULL and color IS NULL and size IS NOT NULL))
);

CREATE TABLE SalesChannel (
    name TEXT PRIMARY KEY,
    type TEXT
);

CREATE TABLE Sells (
    idWine INTEGER REFERENCES PackagedWine(id),
    name TEXT REFERENCES SalesChannel(name),
    quantitySold INTEGER NOT NULL,
    PRIMARY KEY (idWine, name)
);

CREATE VIEW vCultureModeInfluencePrice AS
    SELECT TreatmentAssociation.name, Parcel.typeSoilManagement, Parcel.typePruneManagement, AVG(PackagedWine.price)
    FROM Parcel, TreatmentAssociation, PackagedWine
    WHERE Parcel.idWine = PackagedWine.idWine
        AND Parcel.id = TreatmentAssociation.id
    GROUP BY TreatmentAssociation.name, Parcel.typeSoilManagement, Parcel.typePruneManagement;

CREATE VIEW vClimaticEventInfluencePrice AS
    SELECT ClimaticEvent.date, types.*, ClimaticEventAssociation.intensity, AVG(PackagedWine.price)
    FROM ClimaticEvent, JSON_ARRAY_ELEMENTS_TEXT(ClimaticEvent.types) types, PackagedWine, ClimaticEventAssociation, Parcel
    WHERE Parcel.idWine = PackagedWine.id
        AND Parcel.id = ClimaticEventAssociation.id
        AND ClimaticEventAssociation.date = ClimaticEvent.date
    GROUP BY ClimaticEvent.date, types.*, ClimaticEventAssociation.intensity;

CREATE VIEW vGrapeVarietyInfluenceQuality AS
    SELECT Parcel.nameGrapeVariety, AVG(CAST(Wine.evaluation->>'taste' AS INTEGER)) AS taste, AVG(CAST(Wine.evaluation->>'structure' AS INTEGER)) AS structure, AVG(CAST(Wine.evaluation->>'texture' AS INTEGER)) AS texture, AVG(CAST(Wine.evaluation->>'balance' AS INTEGER)) AS balance
    FROM Parcel, Wine
    WHERE Parcel.idWine = Wine.id
    GROUP BY Parcel.nameGrapeVariety;

CREATE VIEW vCultureModeInfluenceQuality AS
    SELECT TreatmentAssociation.name, Parcel.typeSoilManagement, Parcel.typePruneManagement, AVG(CAST(Wine.evaluation->>'taste' AS INTEGER)) AS taste, AVG(CAST(Wine.evaluation->>'structure' AS INTEGER)) AS structure, AVG(CAST(Wine.evaluation->>'texture' AS INTEGER)) AS texture, AVG(CAST(Wine.evaluation->>'balance' AS INTEGER)) AS balance
    FROM Parcel, TreatmentAssociation, Wine
    WHERE Parcel.idWine = Wine.id
    GROUP BY TreatmentAssociation.name, Parcel.typeSoilManagement, Parcel.typePruneManagement;

CREATE VIEW vSoilManagementInfluenceQuality AS
    SELECT Parcel.typeSoilManagement, AVG(CAST(Wine.evaluation->>'taste' AS INTEGER)) AS taste, AVG(CAST(Wine.evaluation->>'structure' AS INTEGER)) AS structure, AVG(CAST(Wine.evaluation->>'texture' AS INTEGER)) AS texture, AVG(CAST(Wine.evaluation->>'balance' AS INTEGER)) AS balance
    FROM Parcel, Wine
    WHERE Parcel.idWine = Wine.id
    GROUP BY Parcel.typeSoilManagement;

CREATE VIEW vTreatmentAndSoilManagementInfluenceQuality AS
    SELECT TreatmentAssociation.name, Parcel.typeSoilManagement, AVG(CAST(Wine.evaluation->>'taste' AS INTEGER)) AS taste, AVG(CAST(Wine.evaluation->>'structure' AS INTEGER)) AS structure, AVG(CAST(Wine.evaluation->>'texture' AS INTEGER)) AS texture, AVG(CAST(Wine.evaluation->>'balance' AS INTEGER)) AS balance
    FROM TreatmentAssociation, Parcel, Wine
    WHERE TreatmentAssociation.id = Parcel.id 
        AND Parcel.idWine = Wine.id 
    GROUP BY TreatmentAssociation.name, Parcel.typeSoilManagement;

