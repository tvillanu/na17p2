INSERT INTO GrapeVariety VALUES
('Cabernet Sauvignon', 'Cabernet Sauvignon is probably the most famous red wine grape variety on Earth.', 'Red'),
('Pinot Noir', 'Pinot Noir is the dominant red wine grape of Burgundy, now adopted (and extensively studied) in wine regions all over the world.', 'Red'),
('Semillon', 'Semillon is a golden-skinned grape used to make dry and sweet white wines.', 'White');

INSERT INTO SoilManagement VALUES
('Weeded', 'Remove unwanted plants'),
('Grassy', 'Mowed grass');

INSERT INTO PruneManagement VALUES
('Cane pruning', 'Cane pruning involves cutting back about 90 percent of the last year s growth.'),
('Spur pruning', 'Spur pruning involves looking at the new, one-year-old wood growing from the spurs of a cordon.');

INSERT INTO Wine VALUES
(1, 'Bordeaux', 'AOP', 2019, 'Red', 10.6, '{"taste":4,"structure":3,"texture":5,"balance":4}'),
(2, 'Sauternes', 'IGP', 2019, 'White', 5.4, '{"taste":2,"structure":5,"texture":4,"balance":3}');

INSERT INTO Parcel VALUES
(1, 'Sandy', 1.5, 'SOUTH', 2019, 'Semillon', 'Weeded', 'Cane pruning', 2),
(2, 'Silt', 0.75, 'NORTH', 2019, 'Pinot Noir', 'Grassy', 'Cane pruning', 1),
(3, 'Clay', 1.25, 'SOUTH-WEST', 2019, 'Semillon', 'Grassy', 'Spur pruning', 2),
(4, 'Sandy', 0.5, 'EAST', 2019, 'Pinot Noir', 'Weeded', 'Spur pruning', 2),
(5, 'Loam', 2.0, 'NORTH', 2019, 'Pinot Noir', 'Weeded', 'Cane pruning', 1),
(6, 'Sandy', 0.25, 'NORTH-WEST', 2019, 'Semillon', 'Grassy', 'Cane pruning', NULL);

INSERT INTO Treatment VALUES
('Herbicide', 'To kill weeds'),
('Insecticide', 'To kill bugs'),
('Fungicide', 'To get rid of diseases');

INSERT INTO TreatmentAssociation VALUES
('Herbicide', 1),
('Insecticide', 1),
('Fungicide', 1),
('Herbicide', 2),
('Herbicide', 3),
('Insecticide', 4),
('Fungicide', 5);

INSERT INTO ClimaticEvent VALUES
('2019/01/01', '["Snow"]'),
('2019/01/02', '["Heavy rain","Wind"]');

INSERT INTO ClimaticEventAssociation VALUES
('2019/01/01', 2, 1),
('2019/01/02', 2, 3),
('2019/01/01', 3, 5),
('2019/01/01', 4, 4),
('2019/01/01', 5, 2);

INSERT INTO PackagedWine VALUES
(1, 15.2, 'A delicious wine', 0.75, 'Standard', 'Green', NULL, 'WineBottle', 1),
(2, 30.0, 'A delicious wine in a bigger bottle', 1.5, 'Magnum', 'Green', NULL, 'WineBottle', 1),
(3, 5, 'A cheap wine', 0.75, 'Standard', 'Green', NULL, 'WineBottle', 2),
(4, 10, 'An amazing wine', 1.5, NULL, NULL, 3, 'BoxWine', 1),
(5, 5, 'An amazing wine', 1.0, NULL, NULL, 2, 'BoxWine', 2);

INSERT INTO SalesChannel VALUES
('Nicolas', 'Chain store'),
('Carrefour', 'Supermarket');

INSERT INTO Sells VALUES
(1, 'Nicolas', 50),
(4, 'Carrefour', 1000);