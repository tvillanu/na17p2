CREATE USER operator WITH PASSWORD 'azerty';

GRANT SELECT, INSERT, DELETE, UPDATE
ON GrapeVariety, SoilManagement, PruneManagement, Wine,
    Parcel, Treatment, TreatmentAssociation, ClimaticEvent,
    ClimaticEventAssociation, PackagedWine, SalesChannel, Sells
TO operator;

GRANT SELECT
ON vCultureModeInfluencePrice, vClimaticEventInfluencePrice,
    vGrapeVarietyInfluenceQuality, vCultureModeInfluenceQuality,
    vSoilManagementInfluenceQuality, vTreatmentAndSoilManagementInfluenceQuality
TO operator;