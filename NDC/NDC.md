# Note de clarification

## Contexte
Le projet Résilience a pour objectif de mettre en réseau des personnes et des communautés dans une logique d'entre-aide.

## Liste des objets qui devront être gérés dans la base de données

* Parcelle
* Cépage
* Vin
* Vin emballé
* Bouteille de vin (type de Vin emballé)
* Cubi de vin (type de Vin emballé)
* Evaluation
* Circuit de vente
* Evènement climatique
* Gestion du sol
* Gestion de taille
* Traitement

## Liste des propriétés associées à chaque objet

* Parcelle
    * ***unique*** Identifiant : texte
    * Type : texte
    * Surface : décimal
    * Exposition : texte
    * Année : entier
* Cépage :
    * ***unique*** Nom : texte
    * Description : texte
    * Couleur : texte
* Vin :
    * ***unique*** Identifiant : entier
    * nom : texte
    * Millésime : entier
    * Appellation : texte
    * Type de vin : texte
    * Quantité : décimal
* Vin emballé :
    * Prix : décimal
    * Description : texte
    * Volume : décimal
* Bouteille de vin (type de Vin emballé)
    * Hérite des attributs de "Vin emballé"
    * Forme : texte
    * Couleur : texte
* Cubi de vin (type de Vin emballé)
    * Hérite des attributs de "Vin emballé"
    * Format : entier
* Evaluation :
    * Goût : entier de 0 à 5
    * Structure : entier de 0 à 5
    * Texture : entier de 0 à 5
    * Equilibre : entier de 0 à 5
* Circuit de vente :
    * ***unique*** Nom : texte
    * Type : texte
* Evènement climatique :
    * ***unique*** Date : date
    * Type : texte
    * Intensité : entier de 0 à 5
* Gestion du sol :
    * Type : texte
    * Description : texte
* Gestion de taille :
    * Type : texte
    * Description : texte
* Traitement :
    * Nom : texte
    * Description : texte

## Liste des utilisateurs (rôles) appelés à modifier et consulter les données

* Exploitant

## Liste des fonctions que ces utilisateurs pourront effectuer

* Exploitant :
    * Connaître l'influence sur le prix du vin des modes de cultures
    * Connaître l'influence sur le prix du vin des évènements climatiques
    * Connaître l'influence sur la qualité des vins des différents assemblages de terroir
    * Connaître l'influence sur la qualité des vins des différents modes de cultures
    * Connaître l'influence sur la qualité des vins de la gestion du sol (désherbé, enherbé, ...)
    * Connaître l'influence sur la qualité des vins des traitements

## Hypotèses
* Un même vin peut être associé à plusieurs parcelles. Une parcelle est associé à seulement un vin
* Un même vin peut être vendu par le biais de différents circuits de vente
* Avant d'être vendu, un vin doit être emballé. D'où l'ajout d'une classe "Vin emballé" qui se décline en deux catégories : Bouteille de vin et Cubi de vin.
    